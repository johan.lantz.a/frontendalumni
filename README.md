# AlumniNetwork

This is the frontend part of a larger project, created using React. The idea was to create a forum with communication through discussions, events and groups. A platform where people interested in the same topics are able to connect.

## Install
Clone the repository and open the solution.
To run the project you would have to install plugins by typing the following commands in the "Package Manager Console" in Visual Studio Code.
```
npm install
npm install react react-dom
npm install react router-dom
npm install react-hook-form
npm install keycloak-js
npm install -D tailwindcss
```

## Run
To run the project, enter the following command in the "Package Manager Console" in Visual Studio Code:
```
npm start
```

## Authors
#### [Johan Lantz (@johan.lantz.a)](@johan.lantz.a)
#### [Pontus Gillson (@d0senb0den)](@d0senb0den)
#### [Erik Morling (@ermo1222)](@ermo1222)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
