import keycloak from "../../keycloak";
import ProfilePage from "../../pages/ProfilePage";
import GroupPage from "../../pages/GroupPage";
import EventPage from "../../pages/EventPage";
import DiscussionThreadPage from "../../pages/DiscussionThreadPage";
import StartPage from "../../pages/StartPage";
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import KeycloakRoute from "../../routes/KeycloakRoute";
import DB_Login from "../../api/UserApi";
import { useEffect } from "react";
const  userURL = process.env.REACT_APP_USERS;






function Navbar() {

  useEffect( () => {
    if (keycloak.authenticated) {
      console.log("DB Login in progress");
      return () => DB_Login();
    }
  })
  


  return (
    <>
    <nav className="p-3 bg-gray-50 rounded border-gray-200 dark:bg-gray-800 dark:border-gray-700">
      <div className="container flex flex-wrap justify-between items-center mx-auto">
        <h1 className="flex items-center text-7xl block py-2 pr-4 pl-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0
            md:dark:text-white dark:bg-blue-600 md:dark:bg-transparent">Alumni Network</h1>
        <div className="navbar">
        <ul className="list-none m-0 p-0 overflow-hidden">
      <li className="float-right p-4 text-2xl"><section className="actions">
        {!keycloak.authenticated && (
            <button className="block py-2 pr-4 pl-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0
            md:dark:text-white dark:bg-blue-600 md:dark:bg-transparent" aria-current="page" onClick={() => keycloak.login()}>Login</button>
        )}
        {keycloak.authenticated && (
            <button className="block py-2 pr-4 pl-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0
            md:dark:text-white dark:bg-blue-600 md:dark:bg-transparent" aria-current="page" onClick={() => keycloak.logout()}>Logout</button>
        )}
        </section></li>
        <li className="float-left p-3.5 text-2xl">
          <a href="/Home" className="block py-2 pr-4 pl-3 text-white bg-blue-700 rounded md:bg-transparent md:text-blue-700 md:p-0
           md:dark:text-white dark:bg-blue-600 md:dark:bg-transparent" aria-current="page">Home</a>
        </li>
        <li className="float-left p-3.5 text-2xl">
          <a href="/profile" className="block py-2 pr-4 pl-3 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0
           md:hover:text-blue-700 md:p-0 dark:text-gray-400 md:dark:hover:text-white dark:hover:bg-gray-700
            dark:hover:text-white md:dark:hover:bg-transparent">Profile</a>
        </li>
        <li className="float-left p-3.5 text-2xl">
          <a href="/discussionthread" className="block py-2 pr-4 pl-3 text-gray-700 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0
           md:hover:text-blue-700 md:p-0 dark:text-gray-400 md:dark:hover:text-white dark:hover:bg-gray-700
            dark:hover:text-white md:dark:hover:bg-transparent">Threads</a>
        </li>
        <li className="float-left p-3.5 text-2xl">
          <a href="/groups" className="block py-2 pr-4 pl-3 text-gray-700 rounded
           hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-gray-400
            md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Groups</a>
        </li>
        <li className="float-left p-3.5 text-2xl">
          <a href="/Events" className="block py-2 pr-4 pl-3 text-gray-700 rounded
           hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-gray-400
            md:dark:hover:text-white dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent ">Events</a>
        </li>
       </ul>
        
        </div>
      </div>
    </nav>
    <Routes>
          <Route path="/Home" element= {<StartPage/>} /> 
          <Route path="/profile" element= {<KeycloakRoute><ProfilePage/></KeycloakRoute>} /> 
          <Route path="/discussionthread" element= {<KeycloakRoute><DiscussionThreadPage/></KeycloakRoute>} /> 
          <Route path="/Groups" element= {<KeycloakRoute><GroupPage/></KeycloakRoute>} /> 
          <Route path="/Events" element= {<KeycloakRoute><EventPage/></KeycloakRoute>} /> 
        </Routes>
    </>
  );
}




export default Navbar;