import { useForm } from "react-hook-form";
import { PostGroup } from "../../api/GroupApi";




const CreateGroup = () => {

 const { register, handleSubmit, formState: {errors } } = useForm()

    return (
        <>
        <div class="flex items-center justify-center w-96">
        <div class="p-4 w-full max-w-sm content bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
        <form class="space-y-6" onSubmit={ handleSubmit(PostGroup) }>
        <h5 class="text-xl font-medium text-gray-900 dark:text-white">Create a group</h5>
        <div>
            <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Group name</label>
            <input type="text" {...register('name')} placeholder="What's the name of the group?" required="" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
        </div>
        <div>
            <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Description</label>
            <input type="text" {...register('description')} placeholder="Describe your group" 
            class="h-32 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
             focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
              dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required=""/>
        </div>
        <button type="submit" class="w-full break-normal text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Create</button>
    </form>
    </div>
</div>
</>

    )
}

export default CreateGroup