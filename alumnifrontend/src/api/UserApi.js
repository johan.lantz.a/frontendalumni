import { createHeaders } from ".";
import keycloak from "../keycloak";
const  userURL = process.env.REACT_APP_USERS
const kcUserURL = process.env.REACT_APP_KCUSERS



 
  const DB_Login = async() => {
    if (!keycloak.authenticated) {
        return;
    }
    try {
        const response = await fetch(kcUserURL + keycloak.tokenParsed?.sub)
        if (!response.ok) {
            const userDB = await fetch(userURL,{
                method: "POST",
                headers: createHeaders(),
                body: JSON.stringify({
                    keyCloakID: keycloak.tokenParsed?.sub,
                    name: keycloak.tokenParsed.preferred_username
                })
            })
        }
    } catch (error) {
        return[error.message,[]]
    }
}



 const getUsers = async() => {
    

    const response = await fetch(userURL);
    const data = await response.json();
    const threadData = new Array;
    data.forEach(element => {
        threadData.push(element);
    });
    return threadData;
}



 export default DB_Login;