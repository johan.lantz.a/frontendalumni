import { createHeaders } from ".";
const groupURL = "https://experis-alumn-api.herokuapp.com/api/Groups";

export const PostGroup = async(groupText) => {
    try {
        const response = await fetch(groupURL, {
            mode: 'cors',
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                isPrivate: false,
                name: groupText.name,
                description: groupText.description
            })
        });
        if (!response.ok) {
            console.log("Error");
            throw new Error('Could not create group');
        }
        const data = await response.json();
        window.location.reload(false);
        return [null, data];
    }
    catch (error) {
        return [error.message, []];
    }
}


// export const getGroups = async() => {
    

//     const response = await fetch(groupURL);
//     const data = await response.json();
//     const groupData = new Array;
//     data.forEach(element => {
//         groupData.push(element);
//     });

    

//     return groupData;
// }