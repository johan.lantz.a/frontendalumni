import { createHeaders } from ".";
import keycloak from "../keycloak";
import axios from "axios";
import { replyID } from "../pages/DiscussionThreadPage";
const threadURL = process.env.REACT_APP_DISCUSSIONTHREAD
const kcUserURL = process.env.REACT_APP_KCUSERS
const threadPostURL = process.env.REACT_APP_POST


export const PostReply = async(ReplyText) => {
    console.log(ReplyText.text)
    console.log(replyID)

    try {
        const user = await axios.get(kcUserURL + keycloak.tokenParsed?.sub)
        const userID = user.data.id

        const response = await fetch(threadPostURL, {
            mode: 'cors',
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                createdBy: userID,
                text: ReplyText.text,
                isTopLevel: false,
                threadId: replyID
            })
        }
        )
        if (!response.ok) {
            console.log("Error");
            throw new Error('Could not create post');
        }
        const data = await response.json();
        window.location.reload(false);
        return [null, data];
    }
    catch (error) {
        return [error.message, []];
    }
}



export const PostThread = async(postText) => {
    console.log(threadURL);
    console.log(postText.ThreadText);
    console.log(postText.TopicText)
    try {
        const user = await axios.get(kcUserURL + keycloak.tokenParsed?.sub)
        const userID = user.data.id
        console.log(user.data.id)

        const response = await fetch(threadURL, {
            mode: 'cors',
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                createdBy: userID,
                topicId: 1,
                isEvent: false,
                title: postText.TopicText,
                topLevelPost: {
                    createdBy: userID,
                    isTopLevel: true,
                    replyingTo: 0,
                    text: postText.ThreadText
                }
            })
        });
        if (!response.ok) {
            console.log("Error");
            throw new Error('Could not create post');
        }
        const data = await response.json();
        window.location.reload(false);
        return [null, data];
    }
    catch (error) {
        return [error.message, []];
    }
}


export const getThreads = async() => {
    

    const response = await fetch(threadURL);
    const data = await response.json();
    const threadData = new Array;
    data.forEach(element => {
        threadData.push(element);
    });

    

    return threadData;
}
