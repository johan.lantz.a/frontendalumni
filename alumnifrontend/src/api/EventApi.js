import { createHeaders } from ".";
const EventURL = process.env.REACT_APP_EVENTS




export const PostEvent = async(postText) => {
    console.log(EventURL);
    console.log(postText.EventTopic);
    console.log(postText.EventText)
    console.log(postText.StartTime)
    console.log(postText.EndTime)
    try {
        const response = await fetch(EventURL, {
            mode: 'cors',
            method: "POST",
            headers: createHeaders(),
            body: JSON.stringify({
                name: postText.EventTopic,
                description: postText.EventText,
                bannerImg: "",
                StartTime: postText.StartTime,
                EndTime: postText.EndTime
            })
        });
        if (!response.ok) {
            console.log("Error");
            throw new Error('Could not create post');
        }
        const data = await response.json();
        window.location.reload(false);
        return [null, data];
    }
    catch (error) {
        return [error.message, []];
    }
}


export const getEvents = async() => {
    

    const response = await fetch(EventURL);
    const data = await response.json();
    const eventData = new Array;
    data.forEach(element => {
        eventData.push(element);
    });

    

    return eventData;
}
