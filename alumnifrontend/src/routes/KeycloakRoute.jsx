import keycloak from "../keycloak";
import { NavLink } from "react-router-dom";

function KeycloakRoute( { children } ){
  if (keycloak.authenticated) {
    return(
      <>{children}</>
    )
  }else{
    return<>
    <div className="grid place-items-center mt-5">
    <p className="dark:text-white text-3xl">You must be logged in to access this page</p>
    <a className=" inline-block px-6 py-2.5 bg-blue-600 text-white font-medium text-xs leading-tight uppercase rounded shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out " onClick={() => keycloak.login()}>Login</a>
    </div>
    </>
  }
}

export default KeycloakRoute

