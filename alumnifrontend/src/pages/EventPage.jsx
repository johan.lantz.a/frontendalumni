import axios from "axios"
import { useEffect } from "react"
import { useState } from "react"
import CreateEvent from "../components/Events/CreateEvent"

const EventUrl = process.env.REACT_APP_EVENTS

const EventPage = () => {

    const [Events, setEvents] = useState([])

    useEffect( () => {
        fetchdata()
    }, [])

    const fetchdata = async () => {
        const {data} = await axios.get(EventUrl)
        setEvents(data)
    }

    const [show, setshow] = useState(false);
    const onClick = () => {
        if (show) { setshow(false) }
        else if (!show) { setshow(true) }
        }
    var buttonText = show ? "Hide create an event" : "Create an event";


    return(
        <>
        <div class='grid place-items-center '>
        <h1 className='text-5xl mb-5 dark:text-white'>Events</h1>
        <div className='grid place-items-center '>
        <button onClick={onClick} className='mb-5 w-96 break-normal text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'>{buttonText}</button>
        </div>
        <div className='ShowGroup'>
            { show ? <CreateEvent/> : null}
        </div>
        <div class="h-56 grid grid-cols-3 gap-4 content-center">
        { Events.map( ( content ) =>  <div class="m-1 p-4 h-[200%] bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700" 
            key={content.id}>
            <h2 className='text-xl font-medium text-gray-900 dark:text-white hover:underline cursor-pointer' href=''>{content.name}</h2> 
            <h4 className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'>{content.description}</h4>
            <h4 className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'>Starts at: {content.start}</h4>
            </div>  )}
        </div>
        </div>
        </>
    );


}

export default EventPage;