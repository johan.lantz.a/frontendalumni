import CreateDiscussionThread from '../components/Threads/CreateDiscussionThread';
import { Children, useEffect, useState } from 'react';
import axios from 'axios';
import keycloak from '../keycloak';
import Collapsible from 'react-collapsible';
import { data } from 'autoprefixer';
import CreateReply from '../components/Threads/CreateReply';
const ThreadUrl = process.env.REACT_APP_DISCUSSIONTHREAD
const kcUserURL = process.env.REACT_APP_KCUSERS
const threadPostURL = process.env.REACT_APP_THREADPOSTS

export let replyID;

const DiscussionThread = () => {





    const [threads, setThreads] = useState([])
    const [user, setUser] = useState([])
    const [post, setPost] = useState([])
    const [reply, setReply] = useState(false)



    useEffect( () =>{
        fetchdata()
        fetchuser()
    }, [])

    const fetchdata = async () => {
        const {data} = await axios.get(ThreadUrl)
        setThreads(data)
    }

    const fetchuser = async () => {
        const response = await axios.get(kcUserURL + keycloak.tokenParsed?.sub)
        setUser(response.data.name)
    }

    const fetchPosts = async (id) => {
        try { 
            const link = (threadPostURL + id)
            const response = await fetch(link)
             const postdata = await response.json()
             
             setPost(postdata)
            
        //     const {posts} = await axios.get(threadPostURL + id)
        // console.log(posts.data)
         }
         catch (error) { console.log(error)  }
        
    }
    
    const [show, setshow] = useState(false);
    const onClick = () => {
        if (show) { setshow(false) }
        else if (!show) { setshow(true) }
    }
    var buttonText = show ? "Hide post a thread" : "Post a thread";
  

    
    const showReply = () => {
        if (reply) { setReply(false) }
        else if (!reply) {setReply(true) }
    }
    var ReplyText = reply ? "Hide form" : "Reply to this thread"

    return (
        <>
        <div className='grid place-items-center '> 
        <h2 className="text-3xl capitalize dark:text-white">Welcome {user}! </h2>
        </div>
        <br></br>
        <div className='grid place-items-center '>
        <button onClick={onClick} className='w-96 break-normal text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'>{buttonText}</button>
        </div>
        <div className='ShowDiscussionThread'>
            { show ? <CreateDiscussionThread/> : null}
        </div>
        <div class='grid place-items-center '>
        { threads.map( ( content ) =>  <div  class="m-1 p-4 min-w-[50%] max-w-sm content bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700 dark:text-white" 
            key={content.id}>
            <h2 className='text-xl font-medium text-gray-900 dark:text-white hover:underline cursor-pointer' href=''>{content.title}</h2> 
            <h4 className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'>Created by: {content.createdBy}</h4> 
            <Collapsible trigger="View posts" triggerWhenOpen="Close Posts" onOpening={async () => await fetchPosts(content.id)}  className=' dark:text-white'>
                <div className='dark:text-white grid place-items-center'>
                    {post.map( ( post ) => <div key={post.id} className='m-1 p-4 min-w-[50%] max-w-sm content bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 md:p-8 dark:bg-gray-700 dark:border-gray-500'>
                        <h2 className='italic'>Response from:  {post.createdBy}</h2>
                         <p className='text-2xl '>{post.text}</p> 
                        <p className='text-right italic'>Created at: {post.createdAt}</p>
                        </div>
                        )}
                </div>
                <div className='grid place-items-center'>
                <button onClick={ () =>  { showReply();
                {replyID = content.id} }}
                className=' w-96 break-normal text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'>
                    {ReplyText}</button>
                    
                { reply ? <CreateReply></CreateReply> : null }
                </div>
            </Collapsible>
            </div>  )}
        </div>
        
        </>
    )
}

export default DiscussionThread
