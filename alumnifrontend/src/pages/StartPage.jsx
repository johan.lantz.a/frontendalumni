import { useEffect, useState } from "react";
import keycloak from "../keycloak";
import axios from "axios";


export let user


const StartPage = () => {



  return (
    <> 
       <div className='grid place-items-center mt-5'>
        <h1 className='text-4xl dark:text-white'>Welcome to Alumni Network!</h1>
        <h2 className='text-3x1 dark:text-white'>Keep in touch with your friends after your education is done</h2>
        </div>
    </>
  );
}
export default StartPage;
