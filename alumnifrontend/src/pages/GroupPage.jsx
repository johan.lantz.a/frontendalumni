import CreateGroup from '../components/Groups/CreateGroup';
import { useEffect, useState } from 'react';
import axios from 'axios';
const GroupUrl = process.env.REACT_APP_GROUPS


const GroupPage = () => {

    const [groups, setGroups] = useState([])

    useEffect( () => {
        fetchdata()
    }, [])

    const fetchdata = async () => {
        const {data} = await axios.get(GroupUrl)
        setGroups(data)
    }

    const [show, setshow] = useState(false);
    const onClick = () => {
        if (show) { console.log("Here"); setshow(false) }
        else if (!show) { console.log("there"); setshow(true) }
        }
    var buttonText = show ? "Hide create a group" : "Create a group";


    return(
        <>
        <div class='grid place-items-center '>
        <h1 className='text-5xl mb-5 dark:text-white'>Groups</h1>
        <div className='grid place-items-center '>
        <button onClick={onClick} className='mb-5 w-96 break-normal text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'>{buttonText}</button>
        </div>
        <div className='ShowGroup'>
            { show ? <CreateGroup/> : null}
        </div>
        <div class="h-56 grid grid-cols-3 gap-4 content-center mt-40">
        { groups.map( ( content ) =>  <div class="m-1 p-4 h-56 bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700" 
            key={content.id}>
            <h2 className='text-xl font-medium text-gray-900 dark:text-white hover:underline cursor-pointer' href=''>{content.name}</h2> 
            <h4 className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'>{content.description}</h4>
            <h4 className='block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300'>Members: {content.users}</h4>
            </div>  )}
        </div>
        </div>
        </>
    );

}

export default GroupPage;