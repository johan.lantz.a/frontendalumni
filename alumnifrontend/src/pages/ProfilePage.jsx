import keycloak from "../keycloak";
import { data } from "autoprefixer";
import axios from "axios";
import PutUser from "../api/EditUserApi"
import { Children, useEffect, useState } from 'react';
import { useForm } from "react-hook-form";
const kcUserURL = process.env.REACT_APP_KCUSERS;





const ProfilePage = () => {

  const [user, setUser] = useState([])



  
  useEffect( () => {
    fetchuser()
  }, [])

 console.log(user);

 
 const fetchuser = async () => {

  try {
     const response = await axios.get(kcUserURL + keycloak.tokenParsed?.sub)
    setUser(response.data)

  } catch (error) {
    console.log(error);
  }
}

console.log(user);
  
  const { register, handleSubmit, formState: {errors } } = useForm()
  
  return (
      <>
      <p>{user.name}</p>
      <div class="flex items-center justify-center">
      <div class="p-4 w-full max-w-sm content bg-white rounded-lg border border-gray-200 shadow-md sm:p-6 md:p-8 dark:bg-gray-800 dark:border-gray-700">
      <form class="space-y-6" onSubmit={ handleSubmit( PutUser ) }>
      <h5 class="text-xl font-medium text-gray-900 dark:text-white">Update user information</h5>
      <div>
          <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Username</label>
          <input  type="text" defaultValue={user.name}  {...register('Username')} placeholder="Enter your name" required class="
          bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500
           focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" />
      </div>
      <div>
          <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Status</label>
          <input type="text" defaultValue={user.status} {...register('Status')} placeholder="Enter your current work status" 
          class="h-32 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
           focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
            dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required=""/>
      </div>
      <div>
          <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Fun fact</label>
          <input type="text" defaultValue={user.funFact}  {...register('funFact')} placeholder="Enter a fun fact about you" 
          class="h-32 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
           focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
            dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required=""/>
      </div>
      <div>
          <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Img url</label>
          <input type="text" defaultValue={user.img} {...register('img')} placeholder="Enter a url for a profila picture" 
          class="h-32 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
           focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
            dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required=""/>
      </div>
      <div>
          <label htmlFor="text" class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">BIO</label>
          <input type="text" defaultValue={user.bio} {...register('Bio')} placeholder="Enter your Bio" 
          class="h-32 bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg
           focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-600
            dark:border-gray-500 dark:placeholder-gray-400 dark:text-white" required=""/>
      </div>
      <button type="submit" class="w-full break-normal text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Post</button>
  </form>
  </div>
  </div>
 
</>
  )
}
export default ProfilePage;
