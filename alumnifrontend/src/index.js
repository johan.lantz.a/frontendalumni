import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { initialize } from "./keycloak";
import Loading from "./components/loading/loading";

const root = ReactDOM.createRoot(document.getElementById("root"));

// Display a loading screen when connecting to Keycloak
root.render(
  <div className="dark:bg-gray-700 dark:h-screen">
<Loading message="Connecting to Keycloak..." />
</div>)

// Initialize Keycloak
initialize()
  .then(() => { // If No Keycloak Error occurred - Display the App
    root.render(

      <React.StrictMode>
        <div className="dark:bg-gray-700 dark:h-screen">
        <App />
        </div>
      </React.StrictMode>
     
    );
  })
  .catch(() => {
    root.render(
      <React.StrictMode>
         <div className="dark:bg-gray-700 dark:h-screen">
        <p>Could Not Connect To Keycloak.</p>
        </div>
      </React.StrictMode>
    );
  });
